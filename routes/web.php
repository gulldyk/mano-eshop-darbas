<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return redirect('eShop');
// });

Route::resource('/eShop', 'ToyController');

// Route::('/eShop', 'ToysController@login'); 
//     return redirect('eShop');

Route::get('/', function(){ return redirect()->route('eShop.index'); });
Route::get('/home', function(){ return redirect()->route('eShop.index'); });

Auth::routes();

// Route::get('/home', 'HomeController@index');

Route::get('/map', 'MapsController@index');
Route::get('/contact', 'ContactController@index');

Route::resource('/user', 'UserController');

Route::get('/uploadfile','UploadFileController@index');
Route::post('/uploadfile','UploadFileController@showUploadFile');
