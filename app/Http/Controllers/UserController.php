<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware(function($request, $next){
            if(\Auth::guest() || (!\Auth::guest() && \Auth::user()->is_admin == 0)){
                return redirect()->route('login');
           }
        return $next($request);
       });
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \App\User::all();
        // var_dump($users);
        // return view('user.index');
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new \App\User();

        $user->name         = $request->name; 
        $user->surname      = $request->surname;
        $user->email        = $request->email;
         

        $user->save();

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \App\User::find($id);

        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::find($id);
        // var_dump($user);

        return view('user.create', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        // if(!empty($request->password)){
            // jei password laukas nera tuscias - perrasom masyvo 'password' reiksme su jo pacio reiksme tik uzhashinta
            // $data['password'] = \Hash::make($request->password);
        // }else{
            // ismetam 'password' key is masyvo, nes kitaip uzsaugotu kaip tuscia password'a
            // unset($data['password']);
        // }
        
        // atnaujinam user duomenis su modifikuotais duomenimis
        $user = \App\User::find($id)->update($data);

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\User::find($id)->delete();

        return redirect()->route('user.index');
    }
}
