<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ToyController extends Controller
{


    // public function __construct() {
    //     $this->middleware(function($request, $next){
    //         if(\Auth::guest() || (!\Auth::guest() && \Auth::user()->is_admin == 0)){
    //             return redirect()->route('login');
    //        }
    //     return $next($request);
    //    });
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $toys = \App\Toy::all();

        // dd($toys);

        return view('eShop.index', compact('toys'));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eShop.create');
        // return view('movies.create');
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $toy = new \App\Toy();

        $toy->title       = $request->title; 
        $toy->price        = $request->price; 
        $toy->image       = $request->image; 
        $toy->description = $request->description; 

        $toy->save();

        return redirect()->route('eShop.index');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $toy = \App\Toy::find($id);
        // dd($toy);
        return view('eShop.show', compact('toy'));

    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $toy = \App\Toy::find($id);

        return view('eShop.create', compact('toy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \App\Toy::find($id)->update($request->all());

        return redirect()->route('eShop.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Toy::find($id)->delete();

        return redirect()->route('eShop.index');
    }

    // public function login()
    // {
    //     return view('eShop.login');
    // }
}
