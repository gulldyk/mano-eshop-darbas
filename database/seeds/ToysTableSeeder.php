<?php

use Illuminate\Database\Seeder;

class ToysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('toys')->insert([
            'title' => 'Blue Jeep 4x4.',
            'image' => 'Jeep.jpg',
            'description' => 'Vrooom, vrooommmm! Cool motor sounds, revvin tunes, flashy chrome wheels & accents and 12-volts of battery power make Hot Wheels Jeep Wrangler the perfect vehicle for pretend off-road racing adventure! It offers realistic Jeep styling including the Jeep grille, roll bars, and front & back fenders, with the added fun of Hot Wheels colors & graphics. Plus, it has been redesigned with a larger cockpit area and increased wheel base to give kids more legroom and improved stability. Drives two speeds forward (2.5 & 5 mph max.), plus reverse, on hard surfaces and grass, with a high speed lock-out option for beginners and Power-Lock brakes. ',
            'price' => 235,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
        DB::table('toys')->insert([
            'title' => 'Humer',
            'image' => 'hummer.jpg',
            'description' => '"Vrooom, vrooommmm!" Cool motor sounds, revvin tunes, flashy "chrome" wheels & accents and 12-volts of battery power make Hot Wheels Jeep Wrangler the perfect vehicle for pretend "off-road" racing adventure! It offers realistic Jeep styling including the Jeep grille, roll bars, and front & back fenders, with the added fun of Hot Wheels colors & graphics. Plus, it has been redesigned with a larger cockpit area and increased wheel base to give kids more legroom and improved stability. Drives two speeds forward (2.5 & 5 mph max.), plus reverse, on hard surfaces and grass, with a high speed lock-out option for beginners ',
            'price' => 299,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
        DB::table('toys')->insert([
            'title' => 'Ford Ranger',
            'image' => 'Ford-Ranger-Licensed.jpg',
            'description' => '"Vrooom, vrooommmm!" Cool motor sounds, revvin tunes, flashy "chrome" wheels & accents and 12-volts of battery power make Hot Wheels Jeep Wrangler the perfect vehicle for pretend "off-road" racing adventure! It offers realistic Jeep styling including the Jeep grille, roll bars, and front & back fenders, with the added fun of Hot Wheels colors & graphics. Plus, it has been redesigned with a larger cockpit area and increased wheel base to give kids more legroom and improved stability. Drives two speeds forward (2.5 & 5 mph max.), plus reverse, on hard surfaces and grass, with a high speed lock-out option for beginners ',
            'price' => 325,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
        DB::table('toys')->insert([
            'title' => 'Bmw',
            'image' => 'BMW.jpg',
            'description' => 'Our 12v Licensed BMW X6 Ride-on Twin Motor Jeep with parental remote controls is the much anticipated fully licensed BMW jeep which we know our customers have been waiting a long time to emerge. Well here it is and what a ride-on on jeep it is as well, guaranteed to turn heads when ever it and its little driver rocks up in the garden, the park or at school!',
            'price' => 260,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
        DB::table('toys')->insert([
            'title' => 'Range Rover Evogue',
            'image' => 'range rover.jpeg',
            'description' => 'Licensed Range Rover Evoque with LED lights, remote control, 12V motor, and original car paint job. Do not charge more than 3-4 hours.',
            'price' => 299,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
        DB::table('toys')->insert([
            'title' => 'Audi styles Kids',
            'image' => 'audi2.jpg',
            'description' => 'ntroducing the AUDI / BMW / COUPE style ride on. This well-constructed, amazing ride on car has a range of brilliant features. As well as operating the working headlights your child can switch between forward and reverse gears for an authentic driving experience. Reaching speeds of up to 3km/h this stylish electric car features a 6V battery and is available in 4 stunning colours: Red, Black, Pink and White',
            'price' => 199,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
        DB::table('toys')->insert([
            'title' => 'Mercedes ML',
            'image' => '704633.jpg',
            'description' => 'Your young sport SUV fan can roll stylishly in the Avigo Mercedes ML63 12 Volt Powered Ride On - Black. With tons of authentic Mercedes-Benz details, a sleek black paint job and chrome accents, this is a truly exciting ride-on to drive. There is room for two in the realistic interior, and your little driver can enjoy tunes via the MP3 connector and the mega-loud ported speaker system (MP3 player not included). Working headlights and traction strip tires round out the amazing details, along with a high-quality clear windshield and a rear hatch that opens.',
            'price' => 320,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
       DB::table('toys')->insert([
            'title' => 'Corvette',
            'image' => 'corvette.jpg',
            'description' => 'Corvette
				There are no product description',
            'price' => 250,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
      ]);
    }
}
