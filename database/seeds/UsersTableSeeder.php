<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
         'name' => 'Admin',
         'surname' => 'Adm',
         'email' => 'Admin@Admin.com',
         'password' => Hash::make('qwerty'),
       
         'is_admin' => 1,
         'created_at' => new \DateTime(),
         'updated_at' => new \DateTime()
        ]);

        DB::table('users')->insert([
         'name' => 'User',
         'surname' => 'Usr',
         'email' => 'User@User.com',
         'password' => Hash::make('qwerty'),
       
         'is_admin' => 0,
         'created_at' => new \DateTime(),
         'updated_at' => new \DateTime()
        ]);
    }
}
