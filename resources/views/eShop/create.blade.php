@extends("main")

@section("content")

<article>
    <div class="container form-apply">
        <div class="row">
            <div class="col-md-12 ">
            	@if(isset($toy))
            		{{ Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'route' => ['eShop.update', $toy->id]] ) }}
            		<form  method="POST" action="{{ route('eShop.update', $toy->id) }}" class="form-horizontal">
            			{{ Form::hidden('_method', 'put') }}	
            		@else
                	<form class="form-horizontal" action="{{ route('eShop.store') }}" method="POST">
            	@endif
                
                {{ csrf_field() }}
					<fieldset>
						<legend>{{ isset($toy) ? 'Edit product' : 'Add new product' }}</legend>
							<div class="form-group">
								{{ Form::label('Item title', '', ['class' => 'col-md-4 control-label']) }}
								<div class="col-md-4">
									@if(isset($toy))
										{{ Form::text('title', $toy['title'],
										['class' => 'form-control input-md']) }}
									@else
										{{ Form::text('title', '',
										['class' => 'form-control input-md']) }}
									@endif		
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('Price', '', ['class' => 'col-md-4 control-label']) }}
								<div class="col-md-4">
									@if(isset($toy))
										{{ Form::number('price', $toy['price'], 
										['class' => 'form-control input md']) }}
									@else
										{{ Form::number('price', '', 
										['class' => 'form-control input-md']) }}	
									@endif	
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('Description', '', ['class' => 'col-md-4 control-label']) }}
								<div class="col-md-4">
									@if(isset($toy))
										{{ Form::textarea('description', $toy['description'], 
										['rows' => 5, 'class' => 'form-control']) }}                     
									@else
										{{ Form::textarea('description', '', 
										['rows'=> 5, 'class' => 'form-control']) }}	
									@endif	
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('Image URL', '', ['class' => 'col-md-4 control-label']) }}
								<div class="col-md-4">
									@if(isset($toy))
										{{ Form::textarea('image', $toy['image'], 
										['rows' => 5, 'class' => 'form-control']) }}	                     
									@else
										
									@endif	
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('Select the file to upload', '', ['class' => 'col-md-4 control-label']) }}
								<div class="col-md-4">
							<?php
						         echo Form::open(array('url' => '/uploadfile','files'=>'true'));
						       
						         echo Form::file('image');
						         
						         echo Form::close();
						     ?>
								</div>
							</div>

		
							<div class="form-group">
								{{ Form::label('', '', ['class' => 'col-md-4 control-label']) }}
								<div class="col-md-4">
									{{Form::submit('save', ['class' => 'btn btn-inverse'] )}}
								</div>
							</div>
					</fieldset>
				</form>
				<div class="form-group">
					{{form::label('','',['class' => 'col-md-4 control-label'])}}
					<div class="col-md-4">
						@if(isset($toy))
							<form method="POST" action="{{ route('eShop.destroy', $toy->id) }}">
								{{Form::hidden('_method', 'delete')}}
            						{{ csrf_field() }}
            					{{Form::submit('Delete', ['class' => 'btn btn-danger pull-right'])}}
            				</form>
            			@endif
					</div>
				</div>
            </div>
        </div>
    </div> <!-- container main -->   
</article> 
									



@endsection