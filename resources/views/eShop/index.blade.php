@extends("main")

@section("content")
<main>
	<div class="header-bg-image">
      <div class="navigation-text">
         <h1>ride on toys for boys and girls</h1>
            <a class="shop-now-btn" href="#items">View gallery</a>
      </div>
   </div>
	  <div class="container">
	     <div class="row news-bar">
	         <div class="col-md-6 col-xs-12 admin-buttons">
	            <ul>
                  @if(!Auth::guest() && Auth::user()->is_admin)
	                 <li><a href="{{ route('eShop.create') }}">add new item</a></li>
                    <li><a href="{{ URL('uploadfile') }}">upload toy image</a></li>
                  @else
                  @endif
	            </ul>
	         </div>
	      </div>
         <div id="items" class="row container-models">
            @foreach ($toys as $toy)
               <div class="col-md-3 col-sm-12 col-xs-12">
                  <div class="model">
                     <div class="model-description">
                    	   <a href=""><h4>{{ $toy->title }}</h4></a>
                     </div>
                     @if(!Auth::guest())
                        <div>
                           <a href="{{ route('eShop.show', $toy->id) }}"><img class="img-responsive toys-image" src="{{asset('uploads/'. $toy->image) }}" alt="jeep" ></a>
                        </div>
                        <div class="add-to-cart">
                           <a class="fancybox" href="{{'uploads/'. $toy->image }}"><i class="fa fa-search-plus"></i></a>
                        </div>
                     @else
                        <div>
                          <a href="{{ URL::to('login') }}"><img class="img-responsive toys-image" src="{{'uploads/'. $toy->image }}" alt="jeep" ></a>
                        </div>
                        <div class="add-to-cart">
                           <a class="fancybox" href="{{'uploads/'. $toy->image }}"><i class="fa fa-search-plus"></i></a>
                        </div>
                     @endif        
                     <div class="model-price">
                       	<p>{{ $toy->price }} €</p>
                     </div>
                           
                     @if(!Auth::guest() && Auth::user()->is_admin)
                       <button class="btn btn-warning"><a href="{{ route('eShop.edit', $toy->id) }}">Edit item</a></button>

                     @if(isset($toy))
                        <form method="POST" action="{{ route('eShop.destroy', $toy->id) }}">
                           <input type="hidden" name="_method" value="DELETE"><br>
                       {{ csrf_field() }}
                           <button class="btn btn-danger">Delete</button>
                        </form>
                     @else    
                     @endif
                     @endif
                  </div>
               </div>
            @endforeach
        </div> 
    </div> 
</main>
@endsection