@extends("main")

@section("content")

<article>
    <div class="container form-apply">
        <div class="row">
            <div class="col-md-6 log-in-form">
                <form>
               		<div class="existing-user">
                        <h3>Already have account</h3>
                    </div> 
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd">
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"> Remember me</label>
                        </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="col-md-6 log-in-form">
                <form>
                    <div class="new-user">
                        <h3>Create new account</h3>
                    </div> 
                    <div class="form-group">
                        <label for="pwd">Name:</label>
                        <input type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd">
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"> Remember me</label>
                        </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>        
            </div>
        </div>
    </div> <!-- container main -->   
</article> 


@endsection