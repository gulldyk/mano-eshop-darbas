@extends("main")

@section("content")
<div class="container">
  <div id="items" class="row container-models">
      <div class="col-md-6 col-sm-12 col-xs-12">
         <div class="model">
            <div class="model-description">
              	<h3>{{ $toy->title }}</h3>
            </div>
            <div class="model-price">
              	<p>{{ $toy->price }} €</p>
            </div>
            <div>
            	<img class="img-responsive toys-image-description" src="{{asset('uploads/'. $toy->image) }}" alt="jeep" >
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="model">
            <div class="model-description">
              	
            </div>
            <div class="model-price">
              	
            </div>
            <div>
            	<h3>{{ $toy->description }}</h3>
            </div>
         </div>
      </div>
  </div> 
</div>           
                        

                        
                        
                        
     


@endsection


