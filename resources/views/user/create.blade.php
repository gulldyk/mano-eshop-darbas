@extends("main")

@section("content")

<article>
    <div class="container form-apply">
        <div class="row">
            <div class="col-md-12 ">
            	@if(isset($user))
            		<form id="myform"  method="POST" action="{{ route('user.update', $user->id) }}"  class="form-horizontal">
            			<input type="hidden" name="_method" value="put">
            	@else
                	<form class="form-horizontal" action="{{ route('user.store') }}" method="POST">
            	@endif
                
                {{ csrf_field() }}
					<fieldset>
						<legend>{{ isset($user) ? 'Edit users' : 'Create new user' }}</legend>
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Name</label>  
								<div class="col-md-4">
									@if(isset($user))
										<input value="{{ $user['name'] }}" name="name" type="text" placeholder="" class="form-control input-md">
									@else
										<input name="name" type="text" placeholder="" class="form-control input-md">
									@endif		
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Surname</label>  
								<div class="col-md-4">
									@if(isset($user))
										<input value="{{ $user['surname'] }}" name="surname" type="text" placeholder="" class="form-control input-md">
									@else
										<input name="surname" type="text" placeholder="" class="form-control input-md">
									@endif		
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Email</label>  
								<div class="col-md-4">
									@if(isset($user))
										<input value="{{ $user['email'] }}" name="email" type="text" placeholder="" class="form-control input-md">
									@else
										<input name="email" type="text" placeholder="" class="form-control input-md">
									@endif	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput"></label>  
								<div class="col-md-4">
									@if($user->is_admin == 1)
										<input type="hidden" name="is_admin" value="0">
  										<input type="checkbox" checked="" name="is_admin" value="1"> This user has Admin rights<br>
									@else
  										<input type="hidden" name="is_admin" value="0">
  										<input type="checkbox" name="is_admin" value="1"> Make this user Admin
									@endif	
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="singlebutton"></label>
								<div class="col-md-4">
									<input type="submit" name="save" class="btn btn-inverse"></input>
								</div>
							</div>
					</fieldset>
				</form>
			</div>
        </div>
    </div> <!-- container main -->   
</article> 



@endsection