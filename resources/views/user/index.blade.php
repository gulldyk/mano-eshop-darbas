@include("layout.header")



	@yield("content")

	<div class="container form-apply">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
            	<fieldset>
					<legend>Users list</legend>
					<table class="user-table" >
						<tbody>
							<tr>
								<th>Name</th>
								<th>Surname</th>
								<th>Email</th>
								<th>Edit user</th>
								<th>Delete user</th>
							</tr>
							@foreach ($users as $user)
							<tr>
								<td>{{ $user->name }}</td>
								<td>{{ $user->surname }}</td>
								<td>{{ $user->email }}</td>
								<td><a href="{{ route('user.edit', $user->id) }}" class="btn btn-success">Edit</td>
								<td>
							@if(isset($user))
									<form method="POST" action="{{ route('user.destroy', $user->id) }}">
        								<input type="hidden" name="_method" value="DELETE">
        								{{ csrf_field() }}
        								<button class="btn btn-danger">Delete</button>
									</form>
        					@endif	
								</td>
							</tr>
							@endforeach
						</tbody>	
					</table>
				</fieldset>
			</div>
        </div>
    </div> <!-- container main -->   
@include("layout.footer")
								
							








