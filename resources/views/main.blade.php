<!-- header includas-->
@include("layout.header")



@if(session('status'))
    <div class="alert alert-info">{{session('status')}}</div>
@endif
@yield("content")





<!-- footer includas-->
@include("layout.footer")