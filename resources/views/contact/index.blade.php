@extends('main')

@section('content')

<article>
    <div class="container form-apply">
        <div class="row">
            <div class="col-md-4 col-md-12 panel-default">
            	<div class="panel panel-default">
    				<div class="panel-heading">Address</div>
    				<div class="panel-body"><a href="#map">Savanoriu pr., Kaunas</a></div>
  				</div>
			</div>
			<div class="col-md-4 col-md-12 panel-default">
            	<div class="panel panel-default">
    				<div class="panel-heading">E-mail</div>
    				<div class="panel-body"><a href="https://gmail.com" target="blank">Pyppyp@gmail.com</a></div>
  				</div>
			</div>
			<div class="col-md-4 col-md-12 panel-default">
            	<div class="panel panel-default">
    				<div class="panel-heading">Phone</div>
    				<div class="panel-body"><a href="tel:+37061187758">+370 611 87758</a></div>
  				</div>
			</div>
        </div>
        <section>

 		<div id="map" class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2293.194328001599!2d23.944987415923336!3d54.91706198033812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e718f2ace35bb7%3A0x3448be9322898a94!2sSavanori%C5%B3+pr.%2C+Kaunas!5e0!3m2!1sen!2slt!4v1478755136667" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen >
           </iframe>
        </div>
	</section> 
    </div> <!-- container main -->   
</article> 

@endsection
