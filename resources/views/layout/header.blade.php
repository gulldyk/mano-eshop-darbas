<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

        <title>PypPyp</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Acme|Indie+Flower|Nunito" rel="stylesheet">


        <link rel="stylesheet" type="text/css" media="screen" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.css" />
       
        
    </head>
    

<body>
    <header>
        <div class="top-line">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <nav>
                            <ul>
                                <li><a href="{{ route('eShop.index') }}">home</a></li>
                                {{-- <li><a href="{{ URL::to('map') }}">map</a></li> --}}
                                <li><a href="{{ URL::to('contact') }}">contact us</a></li>
                                 
                            @if(!Auth::guest() && Auth::user()->is_admin)
                                <li><a href="#"></a></li>
                                <li><a href="{{ route('user.index') }}">users</a></li>
                            @else
                            @endif

                            </ul>
                        </nav>
                    </div>
                    <div class="shopping-cart">
               
                    @if (Auth::guest())
                    
                        <li><a href="{{ url('/login') }}">login <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
                        <li><a href="{{ url('/register') }}">register <i class="fa fa-user" aria-hidden="true"></i></a></li>

                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} {{ Auth::user()->surname }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                    @endif
                    
                    </div>    
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- top-line -->
        <div class="container header-text">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header-title"><h2>Battery-powered Toys</h2></div>
                </div>
            </div>
            <!-- <div class="navigation">
                <nav>
                    <ul>
                        <li><a href="">jeep</a></li>
                        <li><a href="">motorcycle</a></li>
                        <li><a href="">bat-mobile</a></li> 
                      
                    </ul>
                </nav>
            </div> -->
        </div>
</header>