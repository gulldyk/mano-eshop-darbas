<footer>   
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-3 col-xs-12">
                    <div class="info-box-title"><h4>Information</h4></div>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('eShop.index') }}">Home</a></li>
                        <li><a href="{{ URL::to('contact') }}">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box-title"><h4>Categories</h4></div>
                    <ul class="list-unstyled">
                        <li><a href="">Cars</a></li>
                        <li><a href="">Motorcycles</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="info-box-title"><h4>Extras</h4></div>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('login') }}">Login</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="box-heading info-box-title">
                        <h4>Follow Us</h4>
                        <div class="facebook icon">
                            <a href="https://facebook.com" target="blank" title="Facebook"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
                        </div>
                        <div class="twitter icon">
                            <a href="https://twitter.com" target="blank" title="Twitter"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                        </div>
                        <div class="google-plus icon">
                            <a href="https://plus.google.com" target="blank" title="Google-plus"><i class="fa fa-google-plus fa-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copy-right"><p>Created by <a class="anchor-fix" href="#">MnD </a>2016 &copy</p></div>

<!-- 
    |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ||||  ||||||||||  ||||||||    ||||  |||||||||||  ||       |||||||||  ||||          ||||
    ||||  ||||||||||  ||||||  ||||  |||  |||||||||  |||  ||||||||||||||  ||||||||  ||||||||
    ||||  ||||||||||  |||||  ||||||  |||  |||||||  ||||  ||||||||||||||  ||||||||  ||||||||
    ||||  ||||||||||  |||||  ||||||  ||||  |||||  |||||     |||||||||||  ||||||||  ||||||||
    ||||  ||||||||||  |||||  ||||||  |||||  |||  ||||||  ||||||||||||||  ||||||||  ||||||||
    ||||  ||||||||||  ||||||  ||||  |||||||  |  |||||||  ||||||||||||||  ||||||||  ||||||||
    ||||  ||||||||||      ||||    ||||||||||   ||||||||       |||||||||  ||||||||  ||||||||
    ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
    
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.pack.min.js"></script>
    <script type="text/javascript">
    $(function($){
        var addToAll = false;
        var gallery = false;
        var titlePosition = 'inside';
        $(addToAll ? 'img' : 'img.fancybox').each(function(){
            var $this = $(this);
            var title = $this.attr('title');
            var src = $this.attr('data-big') || $this.attr('src');
            var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
            $this.wrap(a);
        });
        if (gallery)
            $('a.fancybox').attr('rel', 'fancyboxgallery');
        $('a.fancybox').fancybox({
            titlePosition: titlePosition
        });
    });
    $.noConflict();
    </script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    {{-- <script src="js/main.js"></script> --}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>

   
    

<script>

$( "#myform" ).validate({
  rules: {
    name: {
      text: true
    }
  }
});
</script>
</body>
</html>