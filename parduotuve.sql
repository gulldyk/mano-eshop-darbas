-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2016 at 05:00 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parduotuve`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_11_14_154606_create_toys_table', 1),
(4, '2016_11_19_115143_add_isadmin_to_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `toys`
--

CREATE TABLE `toys` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `toys`
--

INSERT INTO `toys` (`id`, `title`, `price`, `image`, `description`, `created_at`, `updated_at`) VALUES
(40, 'Corvette', 250, 'corvette.jpg', 'Corvette\n				There are no product description', '2016-12-06 14:19:02', '2016-12-06 14:19:02'),
(39, 'Mercedes ML', 320, '704633.jpg', 'Your young sport SUV fan can roll stylishly in the Avigo Mercedes ML63 12 Volt Powered Ride On - Black. With tons of authentic Mercedes-Benz details, a sleek black paint job and chrome accents, this is a truly exciting ride-on to drive. There is room for two in the realistic interior, and your little driver can enjoy tunes via the MP3 connector and the mega-loud ported speaker system (MP3 player not included). Working headlights and traction strip tires round out the amazing details, along with a high-quality clear windshield and a rear hatch that opens.', '2016-12-06 14:19:02', '2016-12-06 14:19:02'),
(37, 'Range Rover Evogue', 299, 'range rover.jpeg', 'Licensed Range Rover Evoque with LED lights, remote control, 12V motor, and original car paint job. Do not charge more than 3-4 hours.', '2016-12-06 14:19:02', '2016-12-06 14:19:02'),
(38, 'Audi styles Kids', 199, 'audi2.jpg', 'ntroducing the AUDI / BMW / COUPE style ride on. This well-constructed, amazing ride on car has a range of brilliant features. As well as operating the working headlights your child can switch between forward and reverse gears for an authentic driving experience. Reaching speeds of up to 3km/h this stylish electric car features a 6V battery and is available in 4 stunning colours: Red, Black, Pink and White', '2016-12-06 14:19:02', '2016-12-06 14:19:02'),
(36, 'Bmw', 260, 'BMW.jpg', 'Our 12v Licensed BMW X6 Ride-on Twin Motor Jeep with parental remote controls is the much anticipated fully licensed BMW jeep which we know our customers have been waiting a long time to emerge. Well here it is and what a ride-on on jeep it is as well, guaranteed to turn heads when ever it and its little driver rocks up in the garden, the park or at school!', '2016-12-06 14:19:02', '2016-12-06 14:19:02'),
(35, 'Ford Ranger', 325, 'Ford-Ranger-Licensed.jpg', '"Vrooom, vrooommmm!" Cool motor sounds, revvin tunes, flashy "chrome" wheels & accents and 12-volts of battery power make Hot Wheels Jeep Wrangler the perfect vehicle for pretend "off-road" racing adventure! It offers realistic Jeep styling including the Jeep grille, roll bars, and front & back fenders, with the added fun of Hot Wheels colors & graphics. Plus, it has been redesigned with a larger cockpit area and increased wheel base to give kids more legroom and improved stability. Drives two speeds forward (2.5 & 5 mph max.), plus reverse, on hard surfaces and grass, with a high speed lock-out option for beginners ', '2016-12-06 14:19:02', '2016-12-06 14:19:02'),
(34, 'Humer', 299, 'hummer.jpg', '"Vrooom, vrooommmm!" Cool motor sounds, revvin tunes, flashy "chrome" wheels & accents and 12-volts of battery power make Hot Wheels Jeep Wrangler the perfect vehicle for pretend "off-road" racing adventure! It offers realistic Jeep styling including the Jeep grille, roll bars, and front & back fenders, with the added fun of Hot Wheels colors & graphics. Plus, it has been redesigned with a larger cockpit area and increased wheel base to give kids more legroom and improved stability. Drives two speeds forward (2.5 & 5 mph max.), plus reverse, on hard surfaces and grass, with a high speed lock-out option for beginners ', '2016-12-06 14:19:02', '2016-12-06 14:19:02'),
(33, 'Blue Jeep 4x4.', 235, 'Jeep.jpg', 'Vrooom, vrooommmm! Cool motor sounds, revvin tunes, flashy chrome wheels & accents and 12-volts of battery power make Hot Wheels Jeep Wrangler the perfect vehicle for pretend off-road racing adventure! It offers realistic Jeep styling including the Jeep grille, roll bars, and front & back fenders, with the added fun of Hot Wheels colors & graphics. Plus, it has been redesigned with a larger cockpit area and increased wheel base to give kids more legroom and improved stability. Drives two speeds forward (2.5 & 5 mph max.), plus reverse, on hard surfaces and grass, with a high speed lock-out option for beginners and Power-Lock brakes. ', '2016-12-06 14:19:02', '2016-12-06 14:19:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `is_admin`, `surname`) VALUES
(8, 'User', 'User@User.com', '$2y$10$N6IVKV6qtTfMAkGPl6M0..jGZQxX1MVp/ojRKEb/o4.PwlizN4WQC', 'IU3eUQqqyrIHsXZjbV0oTe7Z5NQ2EPFKbJqzCQL8Yqyz8WTpINLCmfoXh4a4', '2016-12-06 14:38:14', '2016-12-06 14:39:02', 0, 'Usr'),
(7, 'Admin', 'Admin@Admin.com', '$2y$10$vA1AMYWl/uVdeIsMmoGqY.x9Gvf1I92jRv.c48PcO6Rix1TbRBVva', 'QdMXM2RRRzS1nK0F5ucqMjZcoGl0sW7WAk3Zxs2Wzp4GwYwxT0Onmq4zTUfx', '2016-12-06 14:36:21', '2016-12-06 14:51:11', 1, 'Adm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `toys`
--
ALTER TABLE `toys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `toys`
--
ALTER TABLE `toys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
